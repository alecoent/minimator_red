MINIMATOR 1.0
=============

This is MINIMATOR, a tool for controller synthesis and computation of
minimal invariant sets for linear switched systems.

Setup
-----

In order to run the examples included in the distribution, all you
need to do is set an environment variable pointing to the directory
this README is located in:

	export MINIMATOR=/path/to/minimator
	
You can include this in your `.profile` or `.bashrc` if you want.


Dependencies
------------

The control synthesis part of MINIMATOR is implemented in GNU
Octave. The post image operation for the invariant set computation is
done using a separate tool implemented in Ocaml and using the Parma
Polyhedra Library (PPL). A binary is included to save you from the
painful compilation process. For graphical output, the GNU plotting
utils come in handy. 

Here is a list of software tools that MINIMATOR depends on:

* [GNU Octave](http://www.gnu.org/software/octave/)
* [GNU Plotting utils](http://www.gnu.org/software/plotutils/)

Here is a list of software you need for manual compilation:

* [Ocaml](http://ocaml.org/), the development version is 3.12.1 
* [Parma Polyhedra Library](http://bugseng.com/products/ppl/) with Ocaml bindings 
* [GNU Multiprecision library](http://gmplib.org/)
* [mlgmp](http://caml.inria.fr/cgi-bin/hump.en.cgi?contrib=278) GMP bindings for Ocaml 


CUDA extension
--------------

There is an experimental extension that runs the MINIMATOR algoithms
on Nvidia GPUs. In order to use it, go into the cuda subdirectory and
call make. You will need the Nvidia compiler (nvcc) and a full octave
installation for this to work. For a simple example, see the file
examples/twoTanksGrid.m.



More Infos
----------

More information on the tool and on the included benchmarks can be found in the [Wiki](https://bitbucket.org/ukuehne/minimator/wiki).

Have fun!
