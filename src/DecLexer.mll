{
open DecParser
}

rule token = parse
    [' ' '\t']       { token lexbuf }     (* skip blanks *)
  | ['\n' ]          { Lexing.new_line lexbuf; token lexbuf }     (* skip newlines *)

  | "Decomposition"  { DECOMP }
	| "Tile"           { TILE }
	| "number"         { ID }
	| "V"              { BOX }
	| "alpha"          { ALPHA }
	| "beta"           { BETA }
	| "epsilon"        { EPSILON }

	| ['-']?['0'-'9']+'.'['0'-'9']+ as lxm { FLOAT(float_of_string lxm) }
	| ['-']?['0'-'9']+ as lxm { INT(int_of_string lxm) }

	| '-'              { MINUS }
	| '='              { ASSIGN }
	| '('              { LPAREN }
	| ')'              { RPAREN }
	| '{'              { LBRACE }
	| '}'              { RBRACE }
	| ','              { COMMA }
	| ';'              { SEMICOLON }
  | '"'              { QUOTE }
	| eof              { EOF}

	| _ { failwith("Unexpected symbol '" ^ (Lexing.lexeme lexbuf) ^ "'")}

