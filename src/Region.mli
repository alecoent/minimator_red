open Ppl_ocaml
open AffineMap

type region = {
	v : rational_box;   (** region  *)
	f : affine_map;	    (** pattern *)
	e : vec option;     (** error   *)
	n : int;            (** number  *)
}

type decomp = region list

(** make a rational box from a list of intervals (one per dimension) *)
val make_box: (rat * rat) list -> rational_box

(** constructor for region record *)
val make_region: rational_box -> affine_map -> vec option -> int -> region

(** create the initial polyhedra from a decomposition. Returns the boxes *)
(** corresponding to the regions as polyhedra. *)
val init: decomp -> polyhedron list

(** compute the post_delta of a set of polyhedra wrt a decomposition. The 
    bool value indicates if the images are separated from the region borders. *)
val post_delta: decomp -> polyhedron list -> bool * polyhedron list

(** check if the decomposition corresponds to an invariant set *)
val verify_inv: decomp -> bool

(** compute the bounding box for a set of polyhedra *)
val bounding_box: polyhedron list -> rational_box

(** get list of bounds of a bounding box *)
val bounds: rational_box -> (rat * rat) list

(** partition polyhedra wrt to a composition. Assumes that polys do not cut *)
(** region boundaries. *)
val partition: decomp -> polyhedron list -> polyhedron list list

(** simplify image by building bounding boxes for each region *)
val simplify: decomp -> polyhedron list -> polyhedron list

(** simplify image by building bounding boxes for each region and trunc limits *)
val simplify_trunc: decomp -> polyhedron list -> polyhedron list

(** enlarge polyhedra using a vector of error margins *)
val bloat: vec -> polyhedron list -> polyhedron list

(** simplify image by building the convex hull for each region *)
val hull: decomp -> polyhedron list -> polyhedron list

(** print the hull points of a polyhedron *) 
val print_poly: polyhedron -> unit

(** print constraints of a polyhedron *)
val print_constraints: polyhedron -> unit

(** write a 2D plot to a file *)
val plot_polys: string -> polyhedron list -> unit

(** write a 2D plot of the regions to a file *)
val plot_decomp: string -> decomp -> unit