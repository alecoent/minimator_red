open Global
open Region
open DecParser

let parse_file file =
	let in_channel = try (open_in file) with
		| Sys_error e -> print_string ("The file " ^ file ^ " could not be opened.\n" ^ e); exit 0; 
	in
	let lexbuf = try (Lexing.from_channel in_channel) with
		| Failure f -> print_string ("Lexing error in file " ^ file ^ ": " ^ f); exit 0;
	in
	try (
	  DecParser.main DecLexer.token lexbuf
	) with e -> begin
  		let p = lexbuf.Lexing.lex_curr_p in
  		let pos = p.Lexing.pos_cnum in
  		let l = p.Lexing.pos_lnum in
  		let bol = p.Lexing.pos_bol in
  		let c = pos - bol in 
  		print_error (Format.sprintf "Syntax error at line %d char %d" l c);
  		let content = lexbuf.Lexing.lex_buffer in
  		begin
  			try begin
  				let prefix = String.sub content bol c in
  				let next_nl = String.index_from content pos '\n' in  
  				let suffix = String.sub content pos (next_nl - pos) in
  				print_error (prefix ^ suffix);
  				let pointer = String.make (c - 1) '-' in
  				print_error (pointer ^ "^");
  			end with _ -> ();
  	  end;
  		exit 1
		end
