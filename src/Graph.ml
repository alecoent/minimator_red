open AffineMap
open Ppl_ocaml

let next_id = ref 0

type node_t = int * polyhedron

type edge_t = int * int

type t = {
	nodes: node_t list;
	edges: edge_t list;		
	color: (int * int) list;
}

let create_graph () = {
	nodes = [];
	edges = [];		
	color = [];
}

let create_node p = 
	let node = (!next_id, p) in
	next_id := !next_id + 1;
	node
	
let add_node g (i,p) c = {g with 
 	nodes = (i,p) :: g.nodes;
	color = (i,c) :: g.color;
}

let add_edge g (s,t) = {g with edges = (s,t) :: g.edges}


let rec get_color i =
	match i with
	| 0 -> "red"
	| 1	-> "green"
	| 2 -> "blue"
	| 3 -> "magenta"
	| 4 -> "yellow"
	| 5 -> "cyan"
	| 6 -> "orange"
	| 7 -> "brown"
	| j -> get_color (j mod 8)


let reduce g =
	let remove_roots nodes edges =
		List.filter (fun (i,_) ->
			List.exists (fun (_,j) -> i=j) edges
		) nodes in
	let clean_edges nodes edges =
		List.filter (fun (i,j) ->
			(List.exists (fun (x, _) -> x=j) nodes) &&
			(List.exists (fun (x, _) -> x=i) nodes)
		) edges in
	let stop = ref false in
	let tmp_nodes = ref g.nodes in
	let tmp_edges = ref g.edges in
	while not !stop do 
		let rm = remove_roots !tmp_nodes !tmp_edges in
		if (List.length rm) = (List.length !tmp_nodes)  then begin stop := true end;
		tmp_nodes := rm;
		tmp_edges := clean_edges !tmp_nodes !tmp_edges;
	done;
	{g with nodes = !tmp_nodes; edges = !tmp_edges}


let dump g name =
	let dump_nodes file =
  	List.iter (fun (i, _) ->
  		let color = get_color (List.assoc i g.color) in
  		output_string file ((string_of_int i) ^ " [style=filled, fillcolor=\"" ^ color ^ "\"];\n"); 
  	) g.nodes in

	let dump_edges file =
  	List.iter (fun (i,j) ->
  		output_string file ((string_of_int i) ^ " -> " ^ (string_of_int j));
  		output_string file (" [label=\"" ^ (string_of_int (List.assoc i g.color)) ^ "\"];\n"); 
  	) g.edges in
	
	let file = open_out((Global.get_path ()) ^ "/" ^ name ^ ".dot") in
	output_string file "digraph cycles {\n";
	dump_nodes file;
	dump_edges file;
	output_string file "}\n";
	close_out file;

	