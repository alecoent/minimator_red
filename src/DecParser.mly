%{
open Region
open Global
%}

%token <int>   INT
%token <float> FLOAT

%token DECOMP TILE ID BOX ALPHA BETA EPSILON ASSIGN
%token LPAREN RPAREN SEMICOLON MINUS
%token LBRACE RBRACE COMMA QUOTE

%token EOF


%start main             /* the entry point */
%type <Region.decomp> main

%%

/**********************************************/
main:
	decomposition EOF { $1 }
;

decomposition:
  DECOMP LBRACE tiles RBRACE { $3 }
;

tiles:
  | tile       { [$1] }
	| tile tiles { $1 :: $2 }
;

tile:
  TILE LBRACE body RBRACE { $3 }
;

body:
  number SEMICOLON box SEMICOLON alpha SEMICOLON beta SEMICOLON epsilon
	{
		make_region $3 (AffineMap.make $5 $7) $9 $1
	}
;

number:
  ID ASSIGN QUOTE INT QUOTE { $4 }
;

box:
  BOX ASSIGN QUOTE bounds QUOTE { make_box $4 }
;

bounds:
  | bound { [$1] } 
	| bound COMMA bounds  { $1 :: $3 }
;

bound:
  LPAREN coef COMMA coef RPAREN { (Gmp.Q.from_float $2, Gmp.Q.from_float $4) }
;

beta:
  BETA ASSIGN QUOTE vector QUOTE { $4 }
;

epsilon:
	| { None }
	| EPSILON ASSIGN QUOTE vector QUOTE SEMICOLON { Some $4 }
;

vector:
  vec { Array.of_list $1 }
;

vec:
  | coef { [Gmp.Q.from_float $1] }
	| coef vec { (Gmp.Q.from_float $1) :: $2 }
;

alpha:
  ALPHA ASSIGN QUOTE matrix QUOTE { $4 }
;

matrix:
  mat { Array.of_list $1 }
;

mat:
  | vector { [$1] }
	| vector SEMICOLON mat { $1 :: $3 }
;

coef:
  | INT   { float_of_int $1 }
	| FLOAT { $1 }
;