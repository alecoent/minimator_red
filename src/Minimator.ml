open Gmp.Z;;
open Ppl_ocaml;;
open AffineMap;;
open Region;;
open DecParser;;
open Global;;
open Parser;;
open Arg;;
open Graph;;


let arg_outpath    = ref "." in
let arg_iterations = ref 10 in
let arg_simplify   = ref (-1) in
let arg_hull       = ref (-1) in
let arg_plot       = ref false in
let arg_filename   = ref "" in
let arg_trunc      = ref false in
let arg_noerr      = ref false in
let arg_check      = ref false in
let arg_stoponsep  = ref false in

let speclist = [
				("-n", Int (fun i -> arg_iterations := i), " Number of iterations. Default: 10");		
				("-bbox", Int (fun i -> arg_simplify := i) , " Simplify every i iterations by bounding box. Default: do not simplify");
				("-hull", Int (fun i -> arg_hull := i), " Simplify every i iterations by convex hull. Default: do not simplify");
				("-trunc", Set arg_trunc, " Truncate limits during bounding box construction. Default: false");	
				("-noerr", Set arg_noerr, " Do not take epsilon into account. Default: false");	
				("-check", Set arg_check, " Only check that the decomposition induces an invariant set. Default: false");
				("-sep",   Set arg_stoponsep, " Stop on separation of images. Default: false");	
				("-plot", Set arg_plot, " Plot image every iteration. Default: false");
				("-path", Set_string arg_outpath, " Path prefix for output.")
		] in

let filename_set = ref false in		
						
let anon_fun str =
	if !filename_set then begin
		print_error ("Too many arguments: '" ^ str ^ "'");
		exit 1; 
	end else begin
		arg_filename := str;
		filename_set := true
	end in
	
let usage_msg = "Usage: minimator <infile> [options]\n" in


let mkdir dir = 
	try begin
		Unix.mkdir dir 0o775;
	end with Unix.Unix_error (err, s, t) -> begin
		if err = Unix.EEXIST then ()
		else raise (Unix.Unix_error (err, s, t))
	end in

Arg.parse speclist anon_fun usage_msg;
if (!arg_filename = "") then begin
	print_error " input file.";
	Arg.usage speclist usage_msg;
	exit 1	
end;

											
if !arg_outpath <> "." then begin
	mkdir !arg_outpath;
	Global.set_path !arg_outpath;
end; 
mkdir (!arg_outpath ^ "/plot");
let prefix = !arg_outpath ^ "/plot/" in
	
	
let make_plot_script _ =
	let filename = !arg_outpath ^ "/plot_post.sh" in
	let outfile = try (open_out filename) with
		| Sys_error e -> print_string ("The file " ^ filename ^ " could not be opened.\n" ^ e); exit 0; 
	in
	output_string outfile ("for i in " ^ prefix ^ "post_*.plot; do\n");
	output_string outfile ("graph -Tsvg -B -q0.8 -m1 " ^ prefix ^ "decomp -C -m3 -q0.5 $i > " ^ prefix ^ "`basename $i .plot`.svg\n");
	output_string outfile ("graph -Tpng -B -q0.8 -m1 " ^ prefix ^ "decomp -C -m3 -q0.5 $i > " ^ prefix ^ "`basename $i .plot`.png\n");
	output_string outfile ("done\n");
	output_string outfile ("if [ -f \"/usr/bin/convert\" ]; then\n");
  output_string outfile ("convert " ^ prefix ^ "*.png " ^ !arg_outpath ^ "/post_delta.gif\n");
	output_string outfile ("fi\n") in
							
						
let postprocess dec =
	let unset_err r = {r with e = None} in
	if !arg_noerr then 
		List.map unset_err dec 
	else dec in																


let join delim strl =
	match strl with
	| [] -> ""
	| strl -> begin
	  List.fold_left (fun str s ->
  		str ^ delim ^ s  
		) (List.hd strl) (List.tl strl)
		end in


let make_box ps =
	let bbox = Region.bounding_box ps in
	let bounds = Region.bounds bbox in
	let bbstr = 
		join "; " (List.map (fun (l,u) ->
			let lf = Gmp.Q.to_float l
			and uf = Gmp.Q.to_float u in
			(string_of_float lf) ^ ", " ^ (string_of_float uf)			
		) bounds) in
	(bbox, bbstr) in
	
let limit_cycle = ref false in 
			
			
let post dec polys i =
	print_info ("POST^" ^ (string_of_int i));
	let separate, ps = Region.post_delta dec polys in
	limit_cycle := separate;
  print_info ("#V = " ^ (string_of_int (List.length ps)));	
	let num = Printf.sprintf ("%03i") i in
	if !arg_plot then Region.plot_polys (prefix ^ "post_" ^ num ^ ".plot") ps;
	let _, bbstr = make_box ps in
	print_info ("Bounding box: " ^ bbstr);
	ps in
		
			
	
(* LET'S GO ! 	*)
let _ = () in
	let dec = Parser.parse_file !arg_filename in
	let dec = postprocess dec in

	if !arg_check then begin
		let inv = Region.verify_inv dec in
		if inv then begin
			print_info "OK - decomposition is invariant set.";
			exit 0
		end else begin
			print_info "FAIL - decomposition is not invariant.";
			exit 1
		end;
	end;

	let boxes = Region.init dec in
	print_info ("#V = " ^ (string_of_int (List.length boxes)));

	if (!arg_plot) then begin
		Region.plot_polys (prefix ^ "post_000.plot") boxes;
		Region.plot_decomp (prefix ^ "decomp") dec;
	end;
	
	let ps = ref boxes in
	let i = ref 1 in
	let stop = ref false in
	while not !stop do
		ps := post dec !ps !i;
		if !limit_cycle then begin
			print_info "All images are separated. Limit cycle will be reached.";
			stop := !arg_stoponsep;
		end;
		if (!arg_simplify > 0) && (0 = !i mod !arg_simplify) then begin
			print_info "simplify using BOUNDING BOX";
			if !arg_trunc then
				ps := Region.simplify_trunc dec !ps
			else
				ps := Region.simplify dec !ps
		end else if (!arg_hull > 0) && (0 = !i mod !arg_hull) then begin
			print_info "simplify using CONVEX HULL";
			ps := Region.hull dec !ps
		end;
		i := !i + 1;
		stop := !stop || !i >= !arg_iterations
 	done;

	make_plot_script ();

	let _, bbstr = make_box !ps in
	print_string (bbstr);
;;
