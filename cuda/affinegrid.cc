#include <octave/oct.h>
#include <octave/Cell.h>

#include "affinegrid.h"


DEFUN_DLD (affinegrid, args, ,
           "affinegrid (R, A, B, n, l)")
{
  int nargin = args.length ();

  if (nargin != 5)
    print_usage ();
  else {
    NDArray Z = args(0).array_value (); // Rectangular region
    NDArray A = args(1).array_value (); // Square matrices of transf.
    NDArray B = args(2).array_value (); // Vectors of transf.
    int n = args(3).int_value ();       // divide region by nxn
    int l = args(4).int_value ();       // max. length of pattern

    if (error_state){
      printf ("error: invalid input format.\n");
    } else {
      int d = A.rows();     // dimension
      int m = A.cols() / d; // modes
      float z[d*(d+1)];
      float a[d*d*m];
      float b[d*m];
      
      // "serialize" A and B in mode order
      for (int mode=0; mode<m; ++mode){
	for (int i=0; i<d; ++i){
	  b[mode*d + i] = B.elem(i, mode);
	  for (int j=0; j<d; ++j){
	    a[mode*d*d + d*i+j] = A.elem(i,d*mode + j);	  
	  }
	}
      }

      // store zonotope
      for (int i=0; i<d; ++i){
	for (int j=0; j<=d; ++j){
	  z[(d+1)*i+j] = Z.elem(i,j);
	}
      }

      // allocate memory for results of GPU main function
      int grid = n*n;
      int inv[n*n];
      float r[grid*d*(d+1)];
      int pat[l*n*n];

      // call GPU main function
      int status = affine_grid_trans(d, z, a, b, z, inv, r, pat, n, m, l);

      // convert output back to octave format
      octave_value_list ret;
      Cell cell_array(n,n);
      Cell pat_array(n,n);

      Matrix rMat(n, n);
      int zonosize = d*(d+1);
      for (int i=0; i<n; ++i){
	for (int j=0; j<n; ++j){
	  // printf("inv(%i, %i) = %i\n", i, j, inv[n*i+j]);
	  if (inv[n*i+j]) {
	    rMat(i,j) = 1;
	  } else {
	    rMat(i,j) = 0;
	  }
	  Matrix zMat(d, d+1);
	  float* base = r + zonosize*(i + j*n);
	  for (int y=0; y<d; ++y){
	    for (int x=0; x<=d; ++x){
	      zMat(y,x) = base[(d+1)*y+x];
	    }
	  }
	  cell_array(i,j) = zMat;

	  Array<int> pattern(l);
	  for (int p=0; p<l; ++p){
	    pattern(p) = pat[l*(i+n*j)+p];
	  }
	  pat_array(i,j) = pattern;
	}
      }

      ret(0) = rMat;
      ret(1) = cell_array;
      ret(2) = pat_array;
      return ret;
    }
  }

  // invalid input -> return nothing
  return octave_value_list ();
}
