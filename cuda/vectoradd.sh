#!/bin/bash
#PBS -k eo
#PBS -l nodes=01:ppn=1:TCP
#PBS -l walltime=00:01:00
#PBS -l pvmem=1gb

cd /u/kuehne/octupus

echo "----------------------"
echo "vector add on GPU test"

export LD_LIBRARY_PATH=`pwd`:/u/kuehne/local/lib:/u/kuehne/local/lib/octave/3.8.1:$LD_LIBRARY_PATH
time octave test.m &> test.log

echo "----------------------"

