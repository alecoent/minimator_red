
Z = [0,1,0;0,0,1]

A1 = eye(2)
B1 = [-0.2;-0.2]

A2 = eye(2)
B2 = [0.2;0.0]

A3 = eye(2)
B3 = [0.0;0.2]


A = [A1, A2, A3]
B = [B1, B2, B3]

[Inv, Zones, Patterns] = affinegrid(Z,A,B,2,3)
