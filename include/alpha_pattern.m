function alpha_p = alpha_pattern(pattern,zonotope)
	n = length(pattern);
	if n == 1
		alpha_p = alpha_c(pattern(1),zonotope);
	else alpha_p = alpha_c(pattern(n),zonotope) * alpha_pattern(pattern(1:n-1),affine_transfo(zonotope,alpha_c(1,zonotope),beta_c(1,zonotope)));
	endif
endfunction
