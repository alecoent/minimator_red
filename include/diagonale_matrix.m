function vec_diago = diagonale_matrix(matrix);
	for i=1:length(matrix(1,:))
		vec_diago(i,1) = matrix(i,i);
	endfor
endfunction
