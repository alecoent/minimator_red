function Z = str2zon(str)
	intervals = strsplit(str,"*");
	d = length(intervals);
	zonotope = [];
	Z = [];
	for i=1:d
 		interval = strsplit(intervals(i){1}(2:length(intervals(i){1})-1),",");
		Z_line = [(str2num(interval{1})+str2num(interval{2}))/2];
		for j=1:i-1
			Z_line = [Z_line,0];
		endfor
		Z_line = [Z_line,abs(str2num(interval{1})-str2num(interval{2}))/2];
		for j = i+1:d
			Z_line = [Z_line,0];
		endfor
		Z = [Z;Z_line];
	endfor
endfunction

