function is_contr = is_controllable(zonotope_to_control,zonotope_target,tile_num)
	global MaxLength
	global file_to_carto
	global total_pattern_tried;
	pattern = [1];
	admissible_pattern_found = 0;
	is_contr = 0;
	printf("###########################################\nConsidering tile #%d\n###########################################\n",tile_num);
	current_length=0;
	while ((length(pattern) <= MaxLength) && (admissible_pattern_found == 0))
		total_pattern_tried = total_pattern_tried + 1;
		if (is_zonotope_invariant(zonotope_to_control,zonotope_target,pattern) == 1)
#			is_contr = length(pattern);
			admissible_pattern_found = 1;
 			printf("Tile #%d is controllable! by pattern ",tile_num);
			for i=1:length(pattern)
				printf("%d ",pattern(i));
			endfor
			printf("\n");
			output_controller(zonotope_to_control,pattern,tile_num);
			output_dec(zonotope_to_control,pattern,tile_num);
			output_post_dec(zonotope_to_control,pattern,tile_num);
# # 			TIME TO DOT THE IMAGES FOR THE UNFOLDING! # #
# 			if option Unfolding enabled (ne pas oublier qu'il faut changer des trucs avant aussi si unfolding est enabled)
#   			if (length(pattern)>=1)
#   				for i=1:length(pattern)
#   					filename = strcat("./cartos_thermal/carto_thermal_1","zone_",int2str(tile_num),"_",int2str(i));
#   					fprintf(file_to_carto," -m %d -q 0.6 %s",pattern(i)+2,filename);
#   					dot_zonotope(filename,zonotope_to_control);
#   					zonotope_to_control = affine_transfo(zonotope_to_control,alpha_c(pattern(i)),beta_c(pattern(i)));
#   				endfor
#   			endif
		endif
		pattern = next_pattern(pattern);		
	endwhile
	is_contr = admissible_pattern_found;
endfunction
