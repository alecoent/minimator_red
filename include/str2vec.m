function X = str2vec(str)
	X = [];
	str = str(2:length(str)-1);
	X_str = strsplit(str,",");
	X = zeros(length(X_str),1);
	for i=1:length(X_str)
		X(i)=str2num(X_str{i});
	endfor
endfunction
