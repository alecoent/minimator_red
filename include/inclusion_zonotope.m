function inclusion = inclusion_zonotope(zonotope_1,zonotope_2)
	#Zonotope 2 doit etre un "produit d'intervalles"!#
	#On teste l'inclusion de 1 dans 2#
	zonotope_1 = square_zonotope(zonotope_1);
	[C1,G]=zonotope_decompo(zonotope_1);
	[C2,D]=zonotope_decompo(zonotope_2);

	vec_G = diagonale_matrix(G);
	vec_D = diagonale_matrix(D);

	if (abs(C1-C2)+abs(vec_G) <= abs(vec_D))
		inclusion = 1;
	else inclusion = 0;
	endif
endfunction
