function beta_p = beta_pattern(pattern,zonotope)
	n = length(pattern);
	if n == 1
		beta_p = beta_c(pattern(1),zonotope);
	else beta_p = beta_c(pattern(n),zonotope) + alpha_c(pattern(n),zonotope)*beta_pattern(pattern(1:n-1),affine_transfo(zonotope,alpha_c(1,zonotope),beta_c(1,zonotope)));
	endif
endfunction
