function post_plot(Z, P, fps)
  V1 = make_poly(Z);
  plot_poly(fps(P(1)), V1)
  V2 = map_poly(V1, P, fps);
endfunction
