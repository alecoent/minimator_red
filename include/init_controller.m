function init_controller(zonotope)
	global controller_file
	global example_name
	global tau
	[C,G] = zonotope_decompo(zonotope);
	B = C + G*(2*rand(length(G(:,1)),1)-ones(length(G(:,1)),1));

	path_file = pwd();
	i = length(path_file);
	upward = 0;
	while (i > 0)&&(upward ==0)
		if (path_file(i) == '/')
			upward = 1;
		else
			path_file = path_file(1:i-1);
			i--;
		endif
	endwhile


	fprintf(controller_file,strcat("source(\"",pwd(),"/",example_name,"_modes.m\");\n\n"));
	fprintf(controller_file,strcat("source(\"",path_file,"include/is_in_zonotope.m\");\n"));
	fprintf(controller_file,strcat("source(\"",path_file,"include/inclusion_zonotope.m\");\n"));
	fprintf(controller_file,strcat("source(\"",path_file,"include/square_zonotope.m\");\n"));
	fprintf(controller_file,strcat("source(\"",path_file,"include/zonotope_decompo.m\");\n"));
	fprintf(controller_file,strcat("source(\"",path_file,"include/diagonale_matrix.m\");\n"));
	fprintf(controller_file,"function next_X = next_X(X,mode)\n\tnext_X = alpha_c(mode)*X+beta_c(mode);\nendfunction\n\n");
	fprintf(controller_file,"history_X = [];\nFinalTime = 100;\n\n");
	fprintf(controller_file,strcat("X =",mat2str(B),";\n"));
	fprintf(controller_file,strcat("global tau =",mat2str(tau),";\n"));
	fprintf(controller_file,"for i=1:FinalTime\n\tif 1==0\n");
	already_controller = 1;
endfunction
