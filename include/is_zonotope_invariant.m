function flag = is_zonotope_invariant(zonotope_control,zonotope_target,pattern)
  global zonotope_safety;
  global option_safety;

  flag_safe = 1;
  
  for i=1:length(pattern)

    zonotope_control = affine_transfo(zonotope_control,alpha_c(pattern(i),zonotope_control),beta_c(pattern(i),zonotope_control));

    if (option_safety)
      if (!inclusion_zonotope(zonotope_control, zonotope_safety))
	flag_safe = 0;
      endif
    endif
    
  endfor
  flag = inclusion_zonotope(zonotope_control,zonotope_target) && flag_safe;

endfunction;
