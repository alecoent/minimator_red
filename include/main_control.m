function is_contr = main_control()
  global example_name;
  global total_pattern_tried;
  global MaxDepth;
  global file_to_carto;
  global MaxLength;
  global tau;
  global flags;
  [zonotope_control,MaxDepth,MaxLength,flags,tau,xName,yName] = parse_args(argv());

#  disp (zonotope_control);

  tic();

  global outpath = strcat("./",example_name,"/");
  mkdir (outpath);

  filename_sh = strcat(outpath,"plot_dec.sh");
  file_to_carto = fopen(filename_sh, "w");
  fprintf(file_to_carto,cstrcat("graph -T svg -C -X ",xName," -Y ",yName," "));
  filename_controller = strcat(outpath,"controller.m");

  global controller_file = fopen(filename_controller,"w");
  global post_delta_file = fopen(strcat(outpath,example_name,".dec"),"w");
  global post_dec_file   = fopen(strcat(outpath,example_name,"-postdec.m"), "w");
  init_controller(zonotope_control);
  fprintf(post_delta_file,"Decomposition{\n");

  # compute controller
  is_contr = carto_control(zonotope_control, zonotope_control, 0,0,1);
  
  fprintf(post_delta_file,"}");

  output_name = strcat("> ",outpath,example_name,"_decomposition.svg");
  fprintf(file_to_carto,output_name);
  fclose(file_to_carto);
  fclose(post_dec_file);

  elapsed_time = toc();
  printf("Computation realised in %f seconds, trying %d patterns during so\n",elapsed_time,total_pattern_tried);
  system(strcat("sh ./",filename_sh));
  end_controller();
endfunction
