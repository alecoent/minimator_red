function is_contr = carto_control(zonotope_to_control, zonotope_target, depth,tile_num,reach)
	global example_name
	filename = example_name;
	global MaxDepth;
	global file_to_carto;
	global MaxLength;
	global outpath;
	prefix = strcat(outpath, "plot/");
	if exist(strcat(pwd (),filename),"2")==0
	  mkdir(prefix);
	endif
	filename = strcat(prefix,filename,int2str(reach),"_zone_",int2str(tile_num));
	if tile_num >= 0
		is_contr = is_controllable(zonotope_to_control,zonotope_target,tile_num);
	else 
		is_contr = 0;
	endif
	if (is_contr >= 1)
			dot_zonotope(filename,zonotope_to_control);
			fprintf(file_to_carto," -m 2 -q %f %s",is_contr/MaxLength,filename);
	else 	if (depth >= MaxDepth) 
			dot_zonotope(filename,zonotope_to_control);
			fprintf(file_to_carto," -m 1 -q 0.1 %s",filename);
		else 	
			dim = length(zonotope_to_control(:,1));
			printf("Tile #%d subdivised into tiles #%d #%d #%d #%d... \n",tile_num,(2^dim)*tile_num+1,(2^dim)*tile_num+2,(2^dim)*tile_num+3,(2^dim)*tile_num+4);
			is_contr = 1;
			dim = length(zonotope_to_control(:,1));
			for i=0:(2^dim)-1
			        sub_contr = carto_control(split_zonotope(zonotope_to_control,i),zonotope_target,depth + 1,(2^dim)*tile_num + i+1,reach);
				is_contr = is_contr && sub_contr;
			endfor
		endif
	endif
endfunction
