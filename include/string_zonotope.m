function output_string = string_zonotope(zonotope);
	output_string = "[";
	for j=1:length(zonotope(:,1))
		for i=1:length(zonotope(1,:))
			if (i ~= length(zonotope(1,:)))
				to_be_added = sprintf("%f ,",zonotope(j,i));
			else to_be_added = sprintf("%f ",zonotope(j,i));
			endif
			output_string = strcat(output_string,to_be_added);
		endfor
		if (j ~= length(zonotope(:,1)))
			output_string = strcat(output_string,";");
		endif
	endfor
	output_string = strcat(output_string,"]")
endfunction
