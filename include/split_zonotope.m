function morceau = split_zonotope(zonotope,chunk)
	[C,G]=zonotope_decompo(zonotope);
	morceau = [C + 1/2*G*int2bin(chunk,length(C),-1),1/2*G];
endfunction
