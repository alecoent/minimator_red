function dot_zonotope = dot_zonotope(filename,zonotope)
	[C,G] = zonotope_decompo(zonotope);
	if length(C) > 2
		printf("Warning: dot_zonotope requires the case study to be in 2 dimensions.\nYour case study appears to be in %d dimensions\n",length(C));
	else
		extreme_points = [C+G*[-1;-1],C+G*[1;-1],C+G*[1;1],C+G*[-1;1]];
		file_tile = fopen(filename,"w");

		for i=1:length(extreme_points(1,:))
			fprintf(file_tile,"%f %f\n",extreme_points(1,i),extreme_points(2,i));
		endfor
		fprintf(file_tile,"%f %f\n",extreme_points(1,1),extreme_points(2,1));
		fclose(file_tile);
	endif
endfunction 
