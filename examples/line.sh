#!/bin/sh

export BENCH="line"

export MAXLENGTH=4
export MAXDEPTH=5
export R="[-1,1]*[-1,1]"
export TAU=0.2

export POST=10
export HULL=-1
export BBOX=-1

./run.sh
