#!/bin/sh

export BENCH="twoSpirals"

export MAXLENGTH=8
export MAXDEPTH=2
export R="[0.5,2]*[0.5,2]"
export TAU=0.1

export POST=30
export HULL=-1
export BBOX=-1

./run.sh
