#!/bin/sh

export BENCH="VanDerPol"

export MAXLENGTH=5
export MAXDEPTH=3
export R="[-5,5]*[-5,5]"
export TAU=0.1

export POST=30
export HULL=2
export BBOX=-1

./run.sh
