#############################
# CONSTANTS OF THE ANALYSIS #
#############################

global max_depth;
global file_to_carto;
global pattern_max_length;
global total_pattern_tried = 0;


##########################
# CONSTANTS OF THE MODEL #
##########################
global example_name = "VanDerPol";
global numberModes = 1;

function alpha_r = alpha_c(mode,zonotope)
	global tau;
	[C,G] = zonotope_decompo(zonotope);
	alpha_r =[1,tau;-tau,1+tau*(1-C(1)*C(1))];
endfunction

function beta_r = beta_c(mode,zonotope)
	global tau;
	beta_r = [0;0];
endfunction

function zon_error = zon_error(zonotope)
	global tau;
	[C,G] = zonotope_decompo(zonotope);
	zon_error = [0,0;tau*abs(C(1)^2 - (C(1) + abs(G(1,2)) + abs(G(2,2)))^2),0];
endfunction


# Start control synthesis
is_contr = main_control()

if (is_contr == 0) 
  exit(1)
else
  exit(0)
endif
