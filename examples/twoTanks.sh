#!/bin/sh

export BENCH="twoTanks"

export MAXLENGTH=4
export MAXDEPTH=5
export R="[-1.5,2.5]*[-0.5,1.5]"
export TAU=0.2

export POST=15
export HULL=-1
export BBOX=-1

./run.sh
