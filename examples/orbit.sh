#!/bin/sh

export BENCH="orbit"

export MAXLENGTH=10
export MAXDEPTH=4
export R="[-1,1]*[-1,1]"
export TAU=0.5

export POST=10
export HULL=-1
export BBOX=-1

./run.sh
