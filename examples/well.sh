#!/bin/sh

export BENCH="well"

export MAXLENGTH=20
export MAXDEPTH=4
export R="[-1,1]*[-1,1]"
export TAU=0.05

export POST=10
export HULL=-1
export BBOX=-1

./run.sh
