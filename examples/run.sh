#!/bin/sh

LIBDIR=`pwd`/../lib
export LD_LIBRARY_PATH=$LIBDIR:$LD_LIBRARY_PATH

# test if we are called from a parent benchmark script
if [ -z $BENCH ]; then
    echo "[ERR] Name of test case undefined (\$BENCH)."
    echo "[ERR] Use the individual benchmark scripts instead."
    exit 1
fi

# check if environment variable has been set
if [ -z $MINIMATOR ]; then
    echo "[ERR] Environment variable \$MINIMATOR is undefined."
    echo "[ERR] Use 'export \$MINIMATOR=/path/to/minimator' to set it."
    echo "[ERR] You can put this into your .bashrc or .profile if you want."
    exit 1
fi

# clean up 
echo "clean $BENCH/plot"
if [ -d $BENCH/plot ]; then
    rm $BENCH/plot/post_*.png
    rm $BENCH/plot/post_*.plot
fi

# set default values
if [ -z $MINSIMP ]; then
    export MINSIMP=-1
fi

if [ -z $S ]; then
    SAFE=""
else
    SAFE="-S $S"
fi



# Compute decomposition 
octave -p $MINIMATOR/include $BENCH.m -MaxLength $MAXLENGTH -MaxDepth $MAXDEPTH -R $R $SAFE -tau $TAU
if [ $? = 0 ] ; then
    echo "[INF] System is controllable."
else
    echo "[WRN] System is not controllable."
    echo "[INF] Checking if controllable region is invariant."
    $MINIMATOR/bin/minimator $BENCH/$BENCH.dec -check
    if [ $? = 1 ] ; then
	exit 1
    fi
fi

# Perform post_delta on decomposition
$MINIMATOR/bin/minimator $BENCH/$BENCH.dec -n $POST -hull $HULL -bbox $BBOX -plot -path $BENCH -trunc -sep
#$MINIMATOR/bin/minimator $BENCH/$BENCH.dec -n $POST -hull $HULL -bbox $BBOX -plot -path $BENCH -trunc

echo "\n[END]"

# Create plots
sh $BENCH/plot_post.sh
