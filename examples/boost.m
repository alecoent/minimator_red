#############################
# CONSTANTS OF THE ANALYSIS #
#############################

global max_depth;
global file_to_carto;
global pattern_max_length;
global total_pattern_tried = 0;

########################
#    BOOST CONSTANTS   #
########################
global example_name = "boost";
global numberModes = 2;

global x_c = 70;
global x_l = 3;
global r_c = 0.005;
global r_l= 0.05;
global r_0 = 1;
global v_s = 0.8;
global A_1 = [ 	-r_l/x_l , 0
			0			, -1/(x_c*(r_0 + r_c))] ;
global A_2 = [ 	-(r_l + r_0*r_c/(r_0+r_c))/x_l ,  -1/x_l * (r_0/(r_0+r_c)) ;
			1/x_c * (r_0/(r_0+r_c)) ,  -1/x_c * (1/(r_0+r_c))];
global B = [v_s/x_l ; 0] ;

function alpha_r = alpha_c(mode)
	global A_1;
	global A_2;
	global B;
	global tau;

	if (mode == 1)
		alpha_r = expm(A_1*tau);
	elseif (mode == 2)
		alpha_r = expm(A_2*tau);
	endif
endfunction

function beta_r = beta_c(mode)
	global A_1;
	global A_2;
	global B;
	global tau;

	if (mode == 1)
		beta_r = (expm(A_1*tau)-eye(2))*inv(A_1)*B;
	elseif (mode == 2)
		beta_r = (expm(A_2*tau)-eye(2))*inv(A_2)*B;
	endif
endfunction

# Start control synthesis
is_contr = main_control()

if (is_contr == 0) 
  exit(1)
else
  exit(0)
endif
