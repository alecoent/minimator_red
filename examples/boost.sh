#!/bin/sh

export BENCH="boost"

export MAXLENGTH=7
export MAXDEPTH=4
export R="[1.55,2.15]*[1.0,1.4]"
export TAU=0.5

export POST=200
export HULL=10
export BBOX=20
export MINSIMP=20  # do not simplify if less than 20 tiles

./run.sh
